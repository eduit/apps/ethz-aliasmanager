FROM python:3
ARG VCS_TAG

LABEL maintainer="Bengt Giger <bgiger@ethz.ch>"
ENV PYTHONUNBUFFERED=1

COPY main.py requirements.txt /app/
COPY kubeapi /app/kubeapi
COPY dnsapi /app/dnsapi

WORKDIR /app
RUN pip install --proxy=http://proxy.ethz.ch:3128 -r requirements.txt
RUN echo $VCS_TAG >.version.txt

RUN groupadd --system py && useradd --system --gid py py
USER py:py

CMD python3 main.py
