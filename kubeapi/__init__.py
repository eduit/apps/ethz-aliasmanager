from kubernetes import client, config, watch
from kubernetes.client.rest import ApiException
from kubernetes.config.config_exception import ConfigException
from dnsapi import DnsApi
import logging
import os

logger = logging.getLogger("aliasmgr.kubeapi")

triggering_annotation = "alias-managed"


class KubeApi(object):
    """
    Waits for ingress events and reacts with calls to the DNS api, to create and delete aliases
    defined in ingress objects.
    """

    def __init__(self, dns_api):
        """
        Initialize a new Kubernetes API.

        :param dns_api: Implementation of the dnsapi.base class which performs creation and deletion of aliases.
        """
        logger.debug("KubeAPI logger initialized")

        self.dns = DnsApi(dns_api)
        self.trigger = os.getenv("KUBE_TRIGGERING_ANNOTATION", triggering_annotation)

        try:
            config.load_incluster_config()
        except ConfigException:
            logger.error(
                "Could not load Kubernetes configuration. Do we run in Kubernetes?"
            )
            exit(1)

        self.api_instance = client.NetworkingV1Api()
        self.last_version = None

    def init_ingresses(self):
        """
        Get list of ingresses once to read the current resource version. The ingresses read will be skiped
        and the watch will only react on future ingress events.
        """
        api_response = None
        try:
            api_response = self.api_instance.list_ingress_for_all_namespaces(
                pretty="yes", limit=1
            )
        except ApiException as e:
            logger.error(
                "Exception when calling NetworkingV1beta1Api->list_ingress_for_all_namespaces: %s\n"
                % e
            )

        self.last_version = api_response.metadata.resource_version

    def watch_ingresses(self):
        """
        Start watching ingress events. Will be initialized with the latest resource versions, only future
        events will be processed.

        :return: This method never returns.
        """
        vip_host = None
        self.init_ingresses()
        w = watch.Watch()
        f = open("/tmp/healthy", mode="w")  # nosec
        f.close()
        logger.info("Starting to watch ingress events")
        for event in w.stream(
            self.api_instance.list_ingress_for_all_namespaces,
            resource_version=self.last_version,
        ):
            for annotation in event["object"].metadata.annotations.keys():
                # Ingress is asking for an alias
                if annotation == self.trigger:
                    # Check if default VIP is overwritten
                    for annotation_vip in event["object"].metadata.annotations.keys():
                        if annotation_vip == "alias-managed-vip":
                            vip_host = event["object"].metadata.annotations[
                                "alias-managed-vip"
                            ]
                            logger.debug(
                                "VIP overwritten: target is {0}".format(vip_host)
                            )
                            break
                        else:
                            vip_host = None
                    for item in event["object"].spec.rules:
                        logger.debug(
                            "Event: %s %s %s on VIP %s"
                            % (
                                event["type"],
                                event["object"].kind,
                                event["object"].metadata.name,
                                vip_host,
                            )
                        )
                        # No canonical names, only FQDN
                        if item.host == "":
                            logger.warning(
                                "No hostname in ingress {0} found".format(
                                    event["object"].metadata.name
                                )
                            )
                        if "." in item.host:
                            if str(event["type"]) == "DELETED":
                                self.dns.delete_alias(item.host)
                            if str(event["type"]) == "ADDED":
                                logger.debug(
                                    "Calling create_alias with VIP {0}".format(vip_host)
                                )
                                self.dns.create_alias(item.host, vip_host)

        os.remove("/tmp/healthy")  # nosec
        logger.info("Ingress watch stopped")
