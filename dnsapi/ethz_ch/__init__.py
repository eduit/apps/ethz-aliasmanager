from dnsapi.base import ApiBase
import os
import logging
import requests
import time

logger = logging.getLogger("aliasmgr.dnsapi.ethz_ch")

dns_alias_create_template = """
<insert>
  <alias>
    <fqName>{alias}</fqName>
    <hostName>{hostname}</hostName>
    <ttl>{ttl}</ttl>
    <views>
      <view>intern</view>
      <view>extern</view>
    </views>
  </alias>
</insert>
"""

ethz_dns_api_url = "https://www.netcenter.ethz.ch/netcenter/rest/alias/"


class DnsApi(ApiBase):
    """
    Interface to the DNS API service 'netcenter'.

    Mandatory environment variables:

     - DNS_ALIAS_HOSTNAME: name of the host receiving the aliases
     - DNS_USERNAME: account with permissions to modify aliases
     - DNS_PASSWORD: password for DNS_USERNAME
    """

    def __init__(self):
        ApiBase.__init__(self)
        self.api_url = os.getenv("DNS_ALIAS_API", ethz_dns_api_url)
        self.dns_alias_template = os.getenv(
            "DNS_ALIAS_TEMPLATE", dns_alias_create_template
        )
        self.ttl = os.getenv("DNS_ALIAS_TTL", 3200)
        self.hostname = os.getenv("DNS_ALIAS_HOSTNAME")
        logger.debug("Aliases created on {0} by default".format(self.hostname))

        self.username = os.getenv("DNS_USERNAME")
        self.password = os.getenv("DNS_PASSWORD")

        self.session.headers.update({"Content-Type": "text/xml"})

    def create_alias(self, alias, vip_host=None):
        """
        Create an alias

        :param alias: FQDN of the alias to be created
        :param vip_host: Optional overwrite for VIP which gets the alias
        :return: True on success, False otherwise
        """
        retries = 3
        if len(alias) == 0:
            logger.error("Empty alias received, cannot create")
            return

        if vip_host is None or vip_host == "":
            logger.debug(
                "No DNS overwrite, using {0} to assign alias".format(self.hostname)
            )
            vip_host = self.hostname
        else:
            logger.debug("Overwrite active, using {0} to assign alias".format(vip_host))

        payload = self.dns_alias_template.format(
            hostname=vip_host, alias=alias, ttl=self.ttl
        )

        for attempt in range(retries - 1):
            try:
                r = self.session.post(
                    self.api_url, data=payload, auth=(self.username, self.password)
                )
            except requests.ConnectionError:
                logger.error("Could not connect to DNS API service")
                exit(1)

            # Check if the request has succeeded
            if r.status_code != 200:
                logger.error("API call returned error: code {0}".format(r.status_code))
                return False

            # Return code 200 doesn't mean everything went fine
            if "<success>done</success>" in r.text:
                logger.info("Created alias {0} on {1}".format(alias, vip_host))
                return True
            if "The Subdomain has to be defined." in r.text:
                logger.error("Alias not fully qualified.")
                return False
            if "already exists as" in r.text:
                logger.error("Alias already exists.")
                return False
            logger.error("Undefined error: \n\n{0}".format(r.text))
            # undefined error: stay in loop and retry
            time.sleep(1)

    def delete_alias(self, alias):
        """
        Delete an alias

        :param alias: FQDN of the alias to be deleted
        :return: True on success, False otherwise
        """
        try:
            r = self.session.delete(
                self.api_url + alias, auth=(self.username, self.password)
            )
        except requests.ConnectionError:
            logger.error("Could not connect to DNS API service")
            exit(1)
        if r.status_code != 200:
            logger.error("API call returned error: code {0}".format(r.status_code))
            return False

        logger.info("Deleted alias {0}".format(alias))
        return True
        # <errors><error><msg>Neither an ID nor"FQ-Name" haven't been found in DB.</msg></error></errors>
