"""
Wrapper object which load a DNS API class.

API supplied at initialization must implement methods `create_alias`
and `delete_alias` for a certain DNS API.
"""


class DnsApi(object):
    """
    Wrapper object which load a DNS API class.
    """

    def __init__(self, api):
        """
        Initialize with API module
        :param api: Python class derived from dnsapi.base
        """
        self.api = api()

    def create_alias(self, alias, vip_host=None):
        """
        Create an alias

        :param alias: FQDN of the alias to be created
        :param vip_host: Optional overwrite for VIP which gets the alias
        :return: True on success, False otherwise
        """
        self.api.create_alias(alias, vip_host)

    def delete_alias(self, alias):
        """
        Delete an alias

        :param alias: FQDN of the alias to be deleted
        :return: True on success, False otherwise
        """
        self.api.delete_alias(alias)
