import logging
import requests

logger = logging.getLogger("aliasmgr.dnsapi")


class ApiBase(object):
    """
    Base class for DNS API implementations. Implement methods
     - create_alias(alias)
     - delete_alias(alias)
    """

    def __init__(self):
        self.session = requests.Session()

    def create_alias(self, alias, vip_host=None):
        """
        Create an alias. Implement in derived classes.

        :param alias: FQDN of the alias to be created
        :param vip_host: Optional overwrite for VIP which gets the alias
        :return: True on success, False otherwise
        """
        return NotImplementedError("method create_alias must be implemented")

    def delete_alias(self, alias):
        """
        Delete an alias. Implement in derived classes.

        :param alias: FQDN of the alias to be deleted
        :return: True on success, False otherwise
        """
        return NotImplementedError("method delete_alias must be implemented")
