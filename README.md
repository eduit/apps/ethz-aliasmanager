Kubernetes Operator to Manage DNS Aliases
=========================================

Deployed in a Kubernetes cluster, this operator listens to creations and deletions
of ingresses. It uses a DNS API to create and delete corresponding aliases.

Deployment
----------

The directory `deployment/kustomization` provides a base for a kustomization. See
also the content of the `example` directory.

Configuration
-------------

The default Netcenter API has to be configured by environment variables. Mandatory
settings are:

 - `DNS_ALIAS_HOSTNAME`: Where the aliases will be managed, i.e. the hostname of the
   VIP configured for a software load balancer like *MetalLB*.
 - `DNS_USERNAME`, `DNS_PASSWORD`: Credentials with permissions to change aliases for
   the given IP.

Optional settings:

 - `DNS_ALIAS_TTL`: Time to live for aliases created.
 - `DNS_ALIAS_API`: URL of the API service.
 - `DNS_ALIAS_TEMPLATE`: XML which will be sent to the API (see `dnsapi/ethz_ch/`).
 - `LOGLEVEL`: set logging verbosity.
 - `KUBE_TRIGGERING_ANNOTATION`: ingress annotation searched for, defaults to `alias-managed`.

Usage
-----

Per ingress configuration is done by setting annotations:

 - `alias-managed: "true"` enables automatic alias management.
 - `alias-managed-vip: <FDQN>`: overwrite the default VIP.
