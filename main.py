from kubeapi import KubeApi
from dnsapi.ethz_ch import DnsApi
import logging
import os

if __name__ == "__main__":
    logger = logging.getLogger("aliasmgr")
    logger.setLevel(os.getenv("LOGLEVEL", "INFO"))
    ch = logging.StreamHandler()
    formatter = logging.Formatter(
        "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    )
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    api = KubeApi(DnsApi)
    api.watch_ingresses()
